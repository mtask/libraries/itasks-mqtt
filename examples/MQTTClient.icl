module MQTTClient

import iTasks.Extensions.MQTT
import iTasks.Extensions.MQTT.Client
import iTasks.Extensions.MQTT.Util
import iTasks

defaultSettings :: MQTTConnectionSettings
defaultSettings =:
	{ MQTTConnectionSettings
	| host         = "localhost"
	, port         = Port 1883
	, clientId     = "Mqtt client"
	, keepAlive    = 60
	, cleanSession = True
	, auth         = NoAuth
	, lwt          = ?Just (MQTTMsg "last-will-topic" "error" { qos=1, retain=False })
	}

Start world = doTasks demo world

demo :: Task ()
demo = getSettings >>? \conSettings. mqttConnect conSettings controls

getSettings :: Task MQTTConnectionSettings
getSettings = updateInformation [] defaultSettings

controls :: (SimpleSDSLens MQTTClient) -> Task ()
controls sds = anyTask
	[ send sds <<@ Title "Send" @! ()
	, subscribe sds <<@ Title "Subscribe" @! ()
	, unsubscribe sds <<@ Title "Unsubscribe" @! ()
	, received sds <<@ Title "Received" @! ()
	] >>* [OnAction (Action "Disconnect") (always (mqttDisconnect sds >-| controls sds))]

send :: (SimpleSDSLens MQTTClient) -> Task MQTTMsg
send sds = enterInformation [] >>? \msg. mqttSend msg sds >-| send sds

subscribe :: (SimpleSDSLens MQTTClient) -> Task (MQTTTopicFilter, QoS)
subscribe sds = enterInformation [] >>? \sub. mqttSubscribe sub sds >-| subscribe sds

unsubscribe :: (SimpleSDSLens MQTTClient) -> Task MQTTTopicFilter
unsubscribe sds = enterInformation [] >>? \sub. mqttUnsubscribe sub sds >-| unsubscribe sds

received :: (SimpleSDSLens MQTTClient) -> Task [MQTTMsg]
received sds
	# lens = createReceiveLens sds
	= viewSharedInformation [ViewAs (map format)] lens
where format (MQTTMsg t p opts) = "Topic: " +++ t +++ " Payload: " +++ p +++ " Qos: " +++ toString opts.qos +++ " Retained: " +++ toString opts.retain
