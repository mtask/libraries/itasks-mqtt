implementation module iTasks.Extensions.MQTT.Errors

import iTasks

errorCodeToString :: !Int -> String
errorCodeToString 0    = "Success"
errorCodeToString -7   = "Timeout"
errorCodeToString -8   = "Network error"
errorCodeToString -700 = "Incorrect protocol version"
errorCodeToString -701 = "Connection refuced"
errorCodeToString -702 = "Authentication failed"
errorCodeToString erno = "Error code: " +++ (toString erno)
