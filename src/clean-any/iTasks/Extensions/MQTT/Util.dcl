definition module iTasks.Extensions.MQTT.Util

import iTasks
import iTasks.Extensions.MQTT

createReceiveLens :: !(SimpleSDSLens MQTTClient) -> (SimpleSDSLens [MQTTMsg])
mqttSend :: !MQTTMsg !(SimpleSDSLens MQTTClient) -> Task ()
mqttSubscribe :: !MQTTSubscribe !(SimpleSDSLens MQTTClient) -> Task ()
mqttUnsubscribe :: !MQTTTopicFilter !(SimpleSDSLens MQTTClient) -> Task ()
mqttDisconnect :: !(SimpleSDSLens MQTTClient) -> Task ()
