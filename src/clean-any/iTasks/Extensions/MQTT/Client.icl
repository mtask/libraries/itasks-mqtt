implementation module iTasks.Extensions.MQTT.Client

import code from "cWolfMQTTWrapper."

import code from "libwolfmqtt_la-mqtt_client."
import code from "libwolfmqtt_la-mqtt_packet."
import code from "libwolfmqtt_la-mqtt_socket."

from AsyncIO import :: ConnectionId(..)
import Data.Functor
import Data.Func
import qualified Data.Map as DM
import iTasks.Internal.IWorld
import iTasks.Internal.DynamicUtil
import iTasks.Internal.SDS
import iTasks.Internal.Util
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskServer
import iTasks.Extensions.DateTime
import StdDebug
import StdEnv
import System._Pointer
import System.Time
import Text

import iTasks.Extensions._MQTT
import iTasks.Extensions.MQTT.Errors
import iTasks.Extensions.MQTT

mqttConnect :: MQTTConnectionSettings ((SimpleSDSLens MQTTClient) -> Task a) -> Task a | iTask a
mqttConnect conSettings ftask =
	defaultClient >>- \client.
	withShared client \sds.
		(connectionTask conSettings sds >&^ (withSelection (return ()) \ctx->
			    pingBroker sds conSettings.keepAlive ctx
		)) ||- ftask sds

defaultClient :: Task MQTTClient
defaultClient = get currentTimestamp @ \ts->
	{ MQTTClient
	| send = []
	, received = []
	, subscribe = []
	, unsubscribe = []
	, lastMessage = ts
	, disconnect = False
	}

connectionTask :: MQTTConnectionSettings (SimpleSDSLens MQTTClient) -> Task MQTTContext
connectionTask conSettings=:{MQTTConnectionSettings|host,port} sds = Task evalinit
where
	evalinit :: !Event !TaskEvalOpts !*IWorld -> *(!TaskResult MQTTContext, !*IWorld)
	evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event
			= (DestroyedResult, iworld)
		= case addConnection (?Just taskId) host port (wrapIWorldConnectionContext connectionsHandlers sds) iworld of
			(Ok ioHandle, iworld) = eval ioHandle event evalOpts iworld
			(Error e, iworld) = (ExceptionResult e, iworld)

	// This mimics tcpconnect from iTasks/WF/Tasks/IO.icl
	eval :: !IOHandle !Event !TaskEvalOpts !*IWorld -> *(!TaskResult MQTTContext, !*IWorld)
	eval ioHandle event {TaskEvalOpts|taskId} iworld
		= case withIOState ioHandle (\s->Ok s.ioStatus) iworld of
			(Ok (IOConnecting cId), iworld)
				| isDestroyOrInterrupt event
					// Cleaning up the connection and IO State is usually done
					// in the onDestroy handler of the task server but in this
					// case the connection hasn't even been set up yet so it
					// has to be done here
					= (DestroyedResult, closeConnection ioHandle iworld)
				= (ValueResult NoValue mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval ioHandle)), iworld)
			(Ok (IOActive d=:(l :: MQTTContext) cId), iworld)
				| isDestroyOrInterrupt event
					// Set the IOState to destroyed, the engine will clean up the io state
					# (_, iworld) = updIOState ioHandle (\ioState->Ok {ioState & ioStatus=IODestroyed d cId}) iworld
					= (DestroyedResult, iworld)
				= (ValueResult (Value l False) mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval ioHandle)), iworld)
			(Ok (IOClosed (l :: MQTTContext)), iworld)
				// Delete the ioState, the engine doesn't need to clean up anything because the connection was already closed
				# (_, iworld) = delIOState ioHandle iworld
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ValueResult (Value l True) mkTaskEvalInfo (mkUIIfReset event rep) (return 0), iworld)
			(Ok (IOException e), iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception $ "mqttconnect: " +++ e), iworld)
			(Ok ioStatus, iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception ("mqttconnect: corrupt IO task result, expected IOConnecting, IOActive, IOClosed, IOException of type MQTTContext but got: " +++ toString ioStatus)), iworld)
			(Error e, iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception ("mqttconnect: " +++ e)), iworld)

	rep = stringDisplay ("MQTT client " <+++ host <+++ ":" <+++ port)

	// Connection handlers
	connectionsHandlers :: ConnectionHandlersIWorld MQTTContext MQTTClient MQTTClient
	connectionsHandlers =
		{ ConnectionHandlersIWorld
		| onConnect = onConnect
		, onData = onData
		, onShareChange = onShareChange
		, onTick = onTick
		, onDisconnect = onDisconnect
		, onDestroy = onDestroy
		}
	where
		onConnect :: !IOHandle !String !MQTTClient !*IWorld -> *(!MaybeErrorString MQTTContext, !?MQTTClient, ![String], !Bool, !*IWorld)
		onConnect ioHandle addr client iworld
			| not (trace_tn ("onConnect (" +++ toString ioHandle +++ "): " +++ addr)) = undef
			= case withIOState ioHandle (\s->Ok s.ioStatus) iworld of
				(Ok (IOConnecting cId), iworld)
					| not (trace_tn ("creating connection")) = undef
					= case createMQTTConnection conSettings cId iworld of
						(Ok ctx, iworld)
							| not (trace_tn ("done")) = undef
							= (Ok ctx, ?None, [], False, iworld)
						(Error e, iworld)
							| not (trace_tn ("error")) = undef
							= (Error e, ?None, [], True, iworld)
				(Ok s, iworld)
					| not (trace_tn ("unexpected connection state: " +++ toString s)) = undef
					= (Error ("Unexpected connection state for MQTT IO handle: " +++ toString ioHandle +++ " (" +++ toString s +++ ")"), ?None, [], True, iworld)
				(Error e, iworld) = (Error e, ?None, [], True, iworld)

		onData :: !IOHandle !String !MQTTContext !MQTTClient !*IWorld -> *(!MaybeErrorString (?MQTTContext), !?MQTTClient, ![String], !Bool, !*IWorld)
		onData ioHandle data context client iworld
			| not (trace_tn ("onData (" +++ toString ioHandle +++ "): " +++ join ", " [toString (toInt i) \\ i<-:data])) = undef
			# (ret, iworld) = pushBack context data iworld
			| ret == -1 = (Error "Mqtt buffer full!", ?None, [], False, iworld)
			# (ret, iworld) = processCall context iworld
			| ret == 0 = let (c,w) = receive client context iworld in (Ok ?None, ?Just c, [], False, w)
			| otherwise = (Error $ "Mqtt process failed: " +++ errorCodeToString ret, ?None, [], False, iworld)

		onShareChange :: !MQTTContext !MQTTClient !*IWorld -> *(!MaybeErrorString (?MQTTContext), !?MQTTClient, ![String], !Bool, !*IWorld)
		onShareChange context c=:{subscribe=[(topic, qos):tt]} w=:{IWorld|clock}
			| not (trace_tn ("onShareChange")) = undef
			# (ret, w) = (subscribeCall context (packString topic) qos w)
			# c = { c & subscribe = tt, lastMessage = timespecToStamp clock}
			| ret == 0 = let (cl,iw) = receive c context w in (Ok ?None, ?Just cl, [], False, iw)
			| otherwise = (Error $ "Subscription failed: " +++ errorCodeToString ret, ?None, [], False, w)
		onShareChange context c=:{unsubscribe=[topic:tt]} w=:{IWorld|clock}
			# (ret, w) = (unsubscribeCall context (packString topic) w)
			# c = { c & unsubscribe = tt, lastMessage = timespecToStamp clock}
			| ret == 0 = let (cl,iw) = receive c context w in (Ok ?None, ?Just cl, [], False, iw)
			| otherwise =  (Error $ "Unsubscription failed: " +++ errorCodeToString ret, ?None, [], False, w)
		onShareChange context c=:{send=[(MQTTMsg t p opts):ss]} w=:{IWorld|clock}
			# (ret, w) = (publishCall context (packString t) p (size p) opts.qos opts.retain w)
			# c = { c & send = ss, lastMessage = timespecToStamp clock }
			| ret == 0 = let (cl,iw) = receive c context w in (Ok ?None, ?Just cl, [], False, iw)
			| otherwise = (Error $ "Send failed: " +++ errorCodeToString ret, ?None, [], False, w)
		onShareChange _ c=:{disconnect = True} w
			= (Ok ?None, ?None, [], True, w)
		onShareChange _ _ w = (Ok ?None, ?None, [], False, w) // Nothing to do

		onTick :: !MQTTContext !MQTTClient !*IWorld -> *(!MaybeErrorString (?MQTTContext), !?MQTTClient, ![String], !Bool, !*IWorld)
		onTick context client iworld = (Ok ?None, ?None, [], False, iworld)

		onDisconnect :: !IOHandle !MQTTContext !MQTTClient !*IWorld -> *(!MaybeErrorString (?MQTTContext), !?MQTTClient, !*IWorld)
		onDisconnect ioHandle context client iworld
			| not (trace_tn ("onDisconnect (" +++ toString ioHandle +++ "): ")) = undef
			# (_, iworld) = disconnectCall context iworld
			= (Ok ?None, ?None, iworld)

		onDestroy :: !MQTTContext !*IWorld -> *(!MaybeErrorString (?MQTTContext), ![String], !*IWorld)
		onDestroy context iworld
			| not (trace_tn ("onDestroy (): ")) = undef
			# (_, iworld) = disconnectCall context iworld
			= (Ok ?None, [], iworld)

// Ping task
pingBroker :: !(SimpleSDSLens MQTTClient) !Int !MQTTContext -> Task (?Int)
pingBroker sds keepalive context
	| keepalive <= 2 = throw "Keep alive time must be greater than 2 seconds."
	= foreverIf ((==) (?Just 0)) $
		    get sds
		>>- \client. timestampToLocalDateTime (nextFire client.lastMessage)
		>>- \nf. waitForDateTime False nf
		>>- \dt. localDateTimeToTimestamp dt
		>>- \now. get sds
		>>- \client. handlePing client now
where
	nextFire (Timestamp lm) = Timestamp (lm + (keepalive - 2))
	threshold (Timestamp now) (Timestamp lm) = (lm + (keepalive - 2)) - now <= 10
	handlePing client now
		| client.disconnect = return ?None
		| (threshold now client.lastMessage) = sendPing client now
		| otherwise = return (?Just 0)
	sendPing client now
		=   upd (\c. { c & lastMessage = now}) sds
		>-| accWorld (pingCall context)
		>>- \res.case res of
			0   = return (?Just 0)
			ret = throw $ "Failed to send ping: " +++ errorCodeToString ret

// Receive messages if they are available
receive :: !MQTTClient !MQTTContext !*IWorld -> (!MQTTClient, !*IWorld)
receive client context iworld
    # (ret, topic, payload, qos, retain, iworld) = receiveCall context iworld
    # msg = MQTTMsg topic payload { qos=qos, retain=retain }
    | ret == 0 = receive { client & received = [msg:client.received] } context iworld
    | ret == -800 = (client, iworld) // No messages in queue

// Create the MQTT connection
createMQTTConnection :: MQTTConnectionSettings !ConnectionId !*IWorld -> (!MaybeErrorString Int, !*IWorld)
createMQTTConnection conSettings=:{auth,clientId,keepAlive,cleanSession,lwt} (ConnectionId ref) w
	| not (trace_tn ("createMQTTConnection")) = undef
	# (context, ret, w) = call auth w
	= case ret of
		0   = trace_n ("mqtt ctx: " +++ toString context) (Ok context, w)
		ret = (Error $ "Could not connect to the MQTT broker: " +++ errorCodeToString ret , w)
	where
		(MQTTMsg lwt` lwm opts)     = fromMaybe (MQTTMsg "" "" { qos=0, retain=False }) lwt

		call :: !MQTTAuth !*IWorld -> (!Int, !Int, !*IWorld)
		call NoAuth                 w = connectCall ref (packString clientId) keepAlive cleanSession (packString lwt`) (packString lwm) opts.qos opts.retain w
		call (UsernamePassword u p) w = connectAuthCall ref (packString clientId) keepAlive cleanSession (packString u) (packString p) (packString lwt`) (packString lwm) opts.qos opts.retain w

// Interface to WolfMQTT
connectCall :: !Int !{#Char} !Int !Bool !{#Char} !{#Char} !Int !Bool !*IWorld -> *(!Int, !Int, !*IWorld)
connectCall fd ci ka cs lwt lwm qos ret w = code {
	ccall mqtt_connect_without_auth "IsIIssII:pI:A"
}

connectAuthCall :: !Int !{#Char} !Int !Bool !{#Char} !{#Char} !{#Char} !{#Char} !Int !Bool !*IWorld -> *(!Int, !Int, !*IWorld)
connectAuthCall fd ci ka cs u p lwt lwm qos ret w = code {
	ccall mqtt_connect "IsIIssssII:pI:A"
}

disconnectCall :: !Int !*IWorld -> *(!Int, !*IWorld)
disconnectCall c w = code {
	ccall mqtt_disconnect "p:I:A"
}

pingCall :: !Int !*World -> *(!Int, !*World)
pingCall c w = code {
	  ccall mqtt_ping "p:I:A"
}

publishCall :: !Int !{#Char} !{#Char} !Int !Int !Bool !*IWorld -> *(!Int, !*IWorld)
publishCall c topic msg len qos retain w = code {
	  ccall mqtt_publish "pssIII:I:A"
}

subscribeCall :: !Int !{#Char} !Int !*IWorld -> *(!Int, !*IWorld)
subscribeCall c topic qos w = code {
	ccall mqtt_subscribe "psI:I:A"
}

unsubscribeCall :: !Int !{#Char} !*IWorld -> *(!Int, !*IWorld)
unsubscribeCall c topic w = code {
	ccall mqtt_unsubscribe "ps:I:A"
}

processCall :: !Int !*IWorld -> *(!Int, !*IWorld)
processCall c w = code {
	ccall mqtt_process "p:I:A"
}

receiveCall :: !Int !*IWorld -> *(!Int, !String, !String, !Int, !Bool, !*IWorld)
receiveCall c w = code {
	ccall mqtt_receive "I:VISSII:A"
}

pushBack :: !Int !{#Char} !*IWorld -> (!Int, !*IWorld)
pushBack c s w = code {
	ccall mqtt_pushback "IS:I:A"
}
