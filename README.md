# Clean iTask mqtt client using [WolfMQTT](https://github.com/wolfSSL/wolfMQTT)

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`itasks-mqttclient` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).

`itasks-mqttclient` uses WolfMQTT (is statically linked with) which is licensed under GPLv2.
