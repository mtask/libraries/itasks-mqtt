# Changelog

#### v1.0.7

- support iTask 0.16

#### v1.0.6

- fix windows linking problem.
- revert support for iTask 0.14.

#### v1.0.5

- support iTask 0.14.

#### v1.0.4

- support iTask 0.14.

#### v1.0.3

- support iTask 0.7 also

#### v1.0.2

- Rename to `itasks-mqttclient` to really fix url

#### v1.0.1

- Fix url in nitrile.yml

#### v1.0

- Initial nitrile version
